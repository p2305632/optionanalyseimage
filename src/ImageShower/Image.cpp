#include "ImageShower/Image.h"

#include <imgui.h>
#include <GL/gl3w.h>

Image::Image(const cv::Mat& image, const std::string& name, bool canBeClosed)
    : m_Name(name), m_CanBeClosed(canBeClosed)
{
    if (image.type() == CV_8UC1)
        cv::cvtColor(image, m_Data, cv::COLOR_GRAY2RGBA);
    else
        cv::cvtColor(image, m_Data, cv::COLOR_BGR2RGBA);
    m_Texture.create(m_Data.cols, m_Data.rows, m_Data.data);
}

Image::Image(Image&& other)
    : m_Data(std::move(other.m_Data)), m_Name(std::move(other.m_Name)), m_Texture(std::move(other.m_Texture)), m_CanBeClosed(other.m_CanBeClosed)
{
}

Image::~Image()
{

}

const cv::Mat& Image::getData() const
{
    return m_Data;
}

const std::string& Image::getName() const
{
    return m_Name;
}

bool Image::shouldClose() const
{
    return !m_Open;
}

void Image::renderImgui() const
{
    
    if (ImGui::BeginTabItem(m_Name.c_str(), m_CanBeClosed ? &m_Open : nullptr))
    {
        m_Texture.bind();
        ImGui::Image(
            reinterpret_cast<void*>(static_cast<intptr_t>(m_Texture.getID())),
            ImVec2((float)m_Data.cols,
            (float)m_Data.rows)
        );

        ImGui::EndTabItem();
    }
}