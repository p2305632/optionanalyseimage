#include "ImageShower/GLTexture.h"

#include <GL/gl3w.h>

GLTexture::GLTexture()
{

}

GLTexture::GLTexture(unsigned int width, unsigned int height, const void* data)
{
    create(width, height, data);
}

GLTexture::GLTexture(GLTexture&& other)
{
    m_RendererID = other.m_RendererID;
    m_Width = other.m_Width;
    m_Height = other.m_Height;

    other.m_RendererID = 0;
    other.m_Width = 0;
    other.m_Height = 0;
}

GLTexture::~GLTexture()
{
    if (m_RendererID != 0)
        glDeleteTextures(1, &m_RendererID);
}

void GLTexture::create(unsigned int width, unsigned int height, const void* data)
{
    if (m_RendererID != 0)
        glDeleteTextures(1, &m_RendererID);
    
    m_Width = width;
    m_Height = height;

    glGenTextures(1, &m_RendererID);
    bind();
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glPixelStorei(GL_UNPACK_ROW_LENGTH, 0);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, data);
}

unsigned int GLTexture::getID() const
{
    return m_RendererID;
}

void GLTexture::bind() const
{
    glBindTexture(GL_TEXTURE_2D, m_RendererID);
}