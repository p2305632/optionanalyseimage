#include "ImageShower/ImageShower.h"

#include <imgui.h>

ImageShower::ImageShower()
{

}

void ImageShower::addImage(const cv::Mat& image, const std::string& name, bool canBeClosed)
{
    auto it = std::find_if(m_Images.begin(), m_Images.end(), [&name](const auto& img) -> bool { return img.getName() == name; });
    if (it != m_Images.end()) m_Images.erase(it);
    
    m_Images.emplace_back(Image(image, name, canBeClosed));
}

void ImageShower::clear()
{
    m_Images.clear();
}

void ImageShower::renderImgui()
{
    if (m_Images.empty()) return;

    std::vector<decltype(m_Images)::iterator> toDelete;
    
    //ImGui::SetNextWindowSize({0.f, 0.f});
    if (ImGui::Begin("Images"))
    {
        if (ImGui::BeginTabBar("ImagesTab"))
        {
            for (auto it = m_Images.begin(); it != m_Images.end(); ++it)
            {
                const auto& img = *it;

                if (img.shouldClose()) toDelete.push_back(it);
                else img.renderImgui();
            }

            ImGui::EndTabBar();
        }

        ImGui::End();
    }

    for (const auto& it : toDelete)
        m_Images.erase(it);
}