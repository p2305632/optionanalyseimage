#include "State/MainState.h"

#include <imgui.h>

#include "State/TP1State.h"

MainState::MainState()
    : State("Selection du TP")
{

}

MainState::~MainState()
{

}

void MainState::onUpdate()
{
    
}

void MainState::onRenderImgui()
{
    if (ImGui::Begin("Sélection du TP"))
    {
        if (ImGui::Button("TP1"))
            launchTP(1);

        ImGui::End();
    }
}

void MainState::launchTP(int id)
{
    switch (id)
    {
    case 1:
        pushState<TP1State>();
        break;
    default:
        break;
    }
}