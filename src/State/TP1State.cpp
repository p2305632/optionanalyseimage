#include "State/TP1State.h"

#include <imgui.h>

TP1State::TP1State()
    : TPState("TP1")
{

}

void TP1State::onUpdate()
{
}

void TP1State::onRenderImgui()
{
    TPState::onRenderImgui();

    if (m_ImageLoaded && ImGui::Begin("Opérations"))
    {
        if (ImGui::CollapsingHeader("Convolution 2D"))
        {
            ImGui::Checkbox("Use treshold 2D", &m_UseTreshold2D);

            if (m_UseTreshold2D)
                ImGui::SliderFloat("Treshold 2D", &m_FiltersTreshold2D, 0.f, 255.f, "%.0f");

            if (ImGui::Button("Prewitt 2D"))
            {
                edgesDetection2D(Convolution::PREWITT_FILTER_0, Convolution::PREWITT_FILTER_90, &Convolution::EuclideanNormAndSlope, "Contours de Prewitt 2D");
            }

            if (ImGui::Button("Sobel 2D"))
            {
                edgesDetection2D(Convolution::SOBEL_FILTER_0, Convolution::SOBEL_FILTER_90, &Convolution::EuclideanNormAndSlope, "Contours de Sobel 2D");
            }

            if (ImGui::Button("Kirsch 2D"))
            {
                edgesDetection2D(Convolution::KIRSCH_FILTER_0, Convolution::KIRSCH_FILTER_90, &Convolution::EuclideanNormAndSlope, "Contours de Kirsch 2D");
            }
        }
        
        if (ImGui::CollapsingHeader("Convolution 4D"))
        {
            ImGui::Checkbox("Use treshold 4D", &m_UseTreshold4D);

            if (m_UseTreshold4D)
            {
                ImGui::SliderFloat("Treshold 4D", &m_FiltersTreshold4D, 0.f, 255.f, "%.0f");
                ImGui::Checkbox("Color with slopes", &m_ColorWithSlopes4D);
            }

            if (ImGui::Button("Prewitt 4D"))
            {
                edgesDetection4D(
                    Convolution::PREWITT_FILTER_0,
                    Convolution::PREWITT_FILTER_45,
                    Convolution::PREWITT_FILTER_90,
                    Convolution::PREWITT_FILTER_135,
                    &Convolution::MaxAndSlope,
                    "Contours de Prewitt 4D"
                );
            }

            if (ImGui::Button("Sobel 4D"))
            {
                edgesDetection4D(
                    Convolution::SOBEL_FILTER_0,
                    Convolution::SOBEL_FILTER_45,
                    Convolution::SOBEL_FILTER_90,
                    Convolution::SOBEL_FILTER_135,
                    &Convolution::MaxAndSlope,
                    "Contours de Sobel 4D"
                );
            }

            if (ImGui::Button("Kirsch 4D"))
            {
                edgesDetection4D(
                    Convolution::KIRSCH_FILTER_0,
                    Convolution::KIRSCH_FILTER_45,
                    Convolution::KIRSCH_FILTER_90,
                    Convolution::KIRSCH_FILTER_135,
                    &Convolution::MaxAndSlope,
                    "Contours de Kirsch 4D"
                );
            }
        }

        ImGui::End();
    }
}

void TP1State::edgesDetection2D(const cv::Mat& filter1, const cv::Mat& filter2, const Convolution::Pixel2Operand2C& op, const std::string& imageName)
{
    cv::Mat image;
    if (m_ImportedImage.type() == CV_8UC3)
        cv::cvtColor(m_ImportedImage, image, cv::COLOR_RGB2GRAY);
    else
        image = m_ImportedImage;
    
    cv::Mat gradientX = Convolution::Convolve(image, filter1);
    cv::Mat gradientY = Convolution::Convolve(image, filter2);
    cv::Mat normAndSlope = Convolution::OperatePixelPerPixel2C(gradientX, gradientY, op);
    cv::Mat norm, slope;
    cv::extractChannel(normAndSlope, norm, 0);
    cv::extractChannel(normAndSlope, slope, 1);

    cv::Mat render = norm;

    if (m_UseTreshold2D)
    {
        render = Convolution::OperatePixelPerPixel(
            render,
            [&, this](float px) -> float
            {
                return Convolution::Treshold(px, m_FiltersTreshold2D, 0.f, 255.f);
            }
        );
    }
    
    render = Convolution::FloatToUchar(render);

    showImage(render, imageName);
}

void TP1State::edgesDetection4D(
        const cv::Mat& filter1,
        const cv::Mat& filter2,
        const cv::Mat& filter3,
        const cv::Mat& filter4,
        const Convolution::Pixel4Operand2C& op,
        const std::string& imageName)
{
    cv::Mat image;
    if (m_ImportedImage.type() == CV_8UC3)
        cv::cvtColor(m_ImportedImage, image, cv::COLOR_RGB2GRAY);
    else
        image = m_ImportedImage;
    
    cv::Mat gradient_0 = Convolution::Convolve(image, filter1);
    cv::Mat gradient_45 = Convolution::Convolve(image, filter2);
    cv::Mat gradient_90 = Convolution::Convolve(image, filter3);
    cv::Mat gradient_135 = Convolution::Convolve(image, filter4);

    cv::Mat normAndSlope = Convolution::OperatePixelPerPixel2C(gradient_0, gradient_45, gradient_90, gradient_135, op);
    cv::Mat norm, slope;
    cv::extractChannel(normAndSlope, norm, 0);
    cv::extractChannel(normAndSlope, slope, 1);

    cv::Mat render;

    if (m_UseTreshold4D)
    {
        if (m_ColorWithSlopes4D)
        {
            render = cv::Mat(slope.rows, slope.cols, CV_8UC3);

            for (int i = 0; i < render.rows; i++)
            {
                for (int j = 0; j < render.cols; j++)
                {
                    float pxSlope = slope.at<float>(i, j);
                    float pxNorm = norm.at<float>(i, j);

                    pxNorm = Convolution::Treshold(pxNorm, m_FiltersTreshold4D, 0.f, 255.f);

                    float saturation = 180.f / (float)M_PI * pxSlope;
                    
                    // In HSV
                    render.at<cv::Vec3b>(i, j) = { (uchar)saturation, 255, (uchar)pxNorm };
                }
            }

            // Convert to BGR
            cv::cvtColor(render, render, cv::COLOR_HSV2BGR);
        }
        else
        {
            render = Convolution::OperatePixelPerPixel(
                norm,
                [&, this](float px) -> float
                {
                    return Convolution::Treshold(px, m_FiltersTreshold4D, 0.f, 255.f);
                }
            );
            render = Convolution::FloatToUchar(render);
        }
        
    }
    else
        render = Convolution::FloatToUchar(norm);
    
    //render = Convolution::FloatToUchar(render);

    showImage(render, imageName);
}