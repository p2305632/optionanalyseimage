#include "State/TPState.h"

#include <imgui.h>
#include <iostream>
#include <nfd.h>
#include <GL/gl3w.h>

TPState::TPState(const std::string& stateName)
    : State(stateName)
{}

void TPState::onRenderImgui()
{
    if (ImGui::Begin("Project"))
    {
        if (ImGui::Button("Ouvrir une image"))
            openImage();

        ImGui::End();
    }

    m_ImageShower.renderImgui();
}

void TPState::showImage(const cv::Mat& image, const std::string& name, bool canBeClosed)
{
    m_ImageShower.addImage(image, name, canBeClosed);
}

void TPState::openImage()
{
    m_ImageShower.clear();

    nfdchar_t *outPath = NULL;
    nfdresult_t result = NFD_OpenDialog(NULL, NULL, &outPath);
        
    if (result != NFD_OKAY)
    {
        m_ImageLoaded = false;
        return;
    }

    m_ImportedImage = cv::imread( outPath, cv::IMREAD_COLOR );
    if(m_ImportedImage.empty())
    {
        std::cerr << "Couldn't load selected image \"" << outPath << "\"." << std::endl;
        m_ImageLoaded = false;
        return;
    }

    showImage(m_ImportedImage, "Image importée", false);
    m_ImageLoaded = true;
}