#include "State/State.h"

#include <imgui.h>

State::State(const std::string& stateName)
    : m_StateName(stateName)
{}

bool State::hasPushedState() const
{
    return m_PushedState != nullptr;
}

std::unique_ptr<State> State::consumePushedState()
{
    return std::move(m_PushedState);
}

bool State::isPendingKilled() const
{
    return m_IsPendingKilled;
}

void State::kill()
{
    m_IsPendingKilled = true;
}

void State::update()
{
    onUpdate();
}

void State::renderImgui()
{
    ImGui::SetNextWindowSize({200.f, 100.f});
    if (ImGui::Begin(m_StateName.c_str()), nullptr, ImGuiWindowFlags_NoTitleBar)
    {
        if (ImGui::Button("Retour"))
            kill();

        ImGui::End();
    }

    onRenderImgui();
}
