#include "Processing/Convolution.h"

const cv::Mat Convolution::PREWITT_FILTER_0 = 
    (1.f / 3.f) * 
    (
        cv::Mat_<float>(3,3) <<
            1.f, 0.f, -1.f,
            1.f, 0.f, -1.f,
            1.f, 0.f, -1.f
    );
const cv::Mat Convolution::PREWITT_FILTER_45 = 
    (1.f / 3.f) * 
    (
        cv::Mat_<float>(3,3) <<
            0.f, -1.f, -1.f,
            1.f,  0.f, -1.f,
            1.f,  1.f,  0.f
    );
const cv::Mat Convolution::PREWITT_FILTER_90 = 
    (1.f / 3.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -1.f, -1.f, -1.f,
             0.f,  0.f,  0.f,
             1.f,  1.f,  1.f
    );
const cv::Mat Convolution::PREWITT_FILTER_135 = 
    (1.f / 3.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -1.f, -1.f,  0.f,
            -1.f,  0.f,  1.f,
             0.f,  1.f,  1.f
    );
const cv::Mat Convolution::SOBEL_FILTER_0 =
    (1.f / 4.f) * 
    (
        cv::Mat_<float>(3,3) <<
            1.f, 0.f, -1.f,
            2.f, 0.f, -2.f,
            1.f, 0.f, -1.f
    );
const cv::Mat Convolution::SOBEL_FILTER_45 =
    (1.f / 4.f) * 
    (
        cv::Mat_<float>(3,3) <<
            0.f, -1.f, -2.f,
            1.f,  0.f, -1.f,
            2.f,  1.f,  0.f
    );
const cv::Mat Convolution::SOBEL_FILTER_90 =
    (1.f / 4.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -1.f, -2.f, -1.f,
             0.f,  0.f,  0.f,
             1.f,  2.f,  1.f
    );
const cv::Mat Convolution::SOBEL_FILTER_135 =
    (1.f / 4.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -2.f, -1.f,  0.f,
            -1.f,  0.f,  1.f,
             0.f,  1.f,  2.f
    );
const cv::Mat Convolution::KIRSCH_FILTER_0 =
    (1.f / 15.f) * 
    (
        cv::Mat_<float>(3,3) <<
            5.f, -3.f, -3.f,
            5.f,  0.f, -3.f,
            5.f, -3.f, -3.f
    );
const cv::Mat Convolution::KIRSCH_FILTER_45 =
    (1.f / 15.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -3.f, -3.f, -3.f,
             5.f,  0.f, -3.f,
             5.f,  5.f, -3.f
    );
const cv::Mat Convolution::KIRSCH_FILTER_90 =
    (1.f / 15.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -3.f, -3.f, -3.f,
            -3.f,  0.f, -3.f,
             5.f,  5.f,  5.f
    );
const cv::Mat Convolution::KIRSCH_FILTER_135 =
    (1.f / 15.f) * 
    (
        cv::Mat_<float>(3,3) <<
            -3.f, -3.f, -3.f,
            -3.f,  0.f,  5.f,
            -3.f,  5.f,  5.f
    );

cv::Mat Convolution::Convolve(const cv::Mat& image, const cv::Mat& filter)
{
    if (image.rows < 3 || image.cols < 3) return image;
    if (filter.cols != 3 || filter.rows != 3 || filter.type() != CV_32FC1)
    {
        std::cerr << "Filter must be 3x3 floats.\n";
        std::exit(EXIT_FAILURE);
    }

    cv::Mat out(image.size(), CV_32FC1);

    for (int i = 1; i < image.rows - 1; i++)
    {
        for (int j = 1; j < image.cols - 1; j++)
        {
            const uchar& px = image.at<uchar>(i, j);
            out.at<float>(i, j) = 0;

            for (int ii = 0; ii < 3; ii++)
            {
                for (int jj = 0; jj < 3; jj++)
                {
                    const uchar& pxNeighbour = image.at<uchar>(i + ii - 1, j + jj - 1);
                    const float& pxFilter = filter.at<float>(ii, jj);

                    out.at<float>(i, j) += pxFilter * static_cast<float>(pxNeighbour);
                }
            }
        }
    }

    return out;
}

cv::Mat Convolution::OperatePixelPerPixel(const cv::Mat& image1, const cv::Mat& image2, const Pixel2Operand& f)
{
    if (image1.cols != image2.cols || image1.rows != image2.rows)
    {
        std::cerr << "Images must match rows and cols.\n";
        std::exit(EXIT_FAILURE);
    }
    if (image1.type() != CV_32FC1 || image2.type() != CV_32FC1)
    {
        std::cerr << "Images must contain float pixels.\n";
        std::exit(EXIT_FAILURE);
    }
    
    cv::Mat out(image1.size(), CV_32FC1);

    for (int i = 0; i < out.rows; i++)
    {
        for (int j = 0; j < out.cols; j++)
        {
            out.at<float>(i, j) = f(image1.at<float>(i, j), image2.at<float>(i, j));
        }
    }

    return out;
}

cv::Mat Convolution::OperatePixelPerPixel2C(const cv::Mat& image1, const cv::Mat& image2, const Pixel2Operand2C& f)
{
    if (image1.cols != image2.cols || image1.rows != image2.rows)
    {
        std::cerr << "Images must match rows and cols.\n";
        std::exit(EXIT_FAILURE);
    }
    if (image1.type() != CV_32FC1 || image2.type() != CV_32FC1)
    {
        std::cerr << "Images must contain float pixels.\n";
        std::exit(EXIT_FAILURE);
    }
    
    cv::Mat out(image1.rows, image1.cols, CV_32FC2);

    for (int i = 0; i < out.rows; i++)
    {
        for (int j = 0; j < out.cols; j++)
        {
            auto px = f(image1.at<float>(i, j), image2.at<float>(i, j));
            out.at<cv::Vec2f>(i, j) = px;
        }
    }

    return out;
}

cv::Mat Convolution::OperatePixelPerPixel2C(const cv::Mat& image1, const cv::Mat& image2, const cv::Mat& image3, const cv::Mat& image4, const Pixel4Operand2C& f)
{
    if (image1.cols != image2.cols || image1.rows != image2.rows ||
        image3.cols != image2.cols || image3.rows != image2.rows ||
        image3.cols != image4.cols || image3.rows != image4.rows)
    {
        std::cerr << "Images must match rows and cols.\n";
        std::exit(EXIT_FAILURE);
    }
    if (image1.type() != CV_32FC1 || image2.type() != CV_32FC1 || image3.type() != CV_32FC1 || image4.type() != CV_32FC1)
    {
        std::cerr << "Images must contain float pixels.\n";
        std::exit(EXIT_FAILURE);
    }
    
    cv::Mat out(image1.rows, image1.cols, CV_32FC2);

    for (int i = 0; i < out.rows; i++)
    {
        for (int j = 0; j < out.cols; j++)
        {
            auto px = f(image1.at<float>(i, j), image2.at<float>(i, j), image3.at<float>(i, j), image4.at<float>(i, j));
            out.at<cv::Vec2f>(i, j) = px;
        }
    }

    return out;
}

cv::Mat Convolution::OperatePixelPerPixel(const cv::Mat& image, const Pixel1Operand& f)
{
    if (image.type() != CV_32FC1)
    {
        std::cerr << "Image must contain float pixels.\n";
        std::exit(EXIT_FAILURE);
    }

    cv::Mat out(image.size(), CV_32FC1);

    for (int i = 0; i < image.rows; i++)
    {
        for (int j = 0; j < image.cols; j++)
        {
            out.at<float>(i, j) = f(image.at<float>(i, j));
        }
    }

    return out;
}

cv::Mat Convolution::UcharToFloat(const cv::Mat& image)
{
    if (image.type() != CV_8UC1)
    {
        std::cerr << "Image must contain uchar pixels.\n";
        std::exit(EXIT_FAILURE);
    }

    cv::Mat out(image.size(), CV_32FC1);

    for (int i = 0; i < image.rows; i++)
    {
        for (int j = 0; j < image.cols; j++)
        {
            out.at<float>(i, j) = static_cast<float>(image.at<uchar>(i, j));
        }
    }

    return out;
}

cv::Mat Convolution::FloatToUchar(const cv::Mat& image)
{
    if (image.type() != CV_32FC1)
    {
        std::cerr << "Image must contain float pixels.\n";
        std::exit(EXIT_FAILURE);
    }

    cv::Mat out(image.size(), CV_8UC1);

    for (int i = 0; i < image.rows; i++)
    {
        for (int j = 0; j < image.cols; j++)
        {
            float px = image.at<float>(i, j);
            px = ClampDown(px, 0.f);
            
            out.at<uchar>(i, j) = static_cast<uchar>(px);
        }
    }

    return out;
}

float Convolution::EuclideanNorm(float left, float right)
{
    return std::sqrtf(left * left + right * right);
}

cv::Vec2f Convolution::EuclideanNormAndSlope(float x, float y)
{
    return { EuclideanNorm(x, y), y / x };
}

float Convolution::Max(float left, float right)
{
    return std::fmaxf(left, right);
}

cv::Vec2f Convolution::MaxAndSlope(float px_0, float px_45, float px_90, float px_135)
{
    float maxVal = std::fmaxf(px_0, px_45);
    maxVal = std::fmaxf(maxVal, px_90);
    maxVal = std::fmaxf(maxVal, px_135);

    if (maxVal == px_0) return { maxVal, 0.f };
    if (maxVal == px_45) return { maxVal, (float)M_PI / 4.f };
    if (maxVal == px_90) return { maxVal, (float)M_PI / 2.f };
    return { maxVal, (float)M_PI * 3.f / 4.f };
}

float Convolution::ManhattanDistance(float left, float right)
{
    return std::fabsf(left) + std::fabsf(right);
}

float Convolution::ClampPixel(float pixel, float min, float max)
{
    if (pixel < min) return min;
    if (pixel > max) return max;
    return pixel;
}

float Convolution::ClampDown(float pixel, float min)
{
    if (pixel < min) return min;
    return pixel;
}

float Convolution::ClampUp(float pixel, float max)
{
    if (pixel > max) return max;
    return pixel;
}

float Convolution::Treshold(float pixel, float treshold, float lowValue, float highValue)
{
    if (pixel < treshold) return lowValue;
    else return highValue;
}