#include <iostream>
#include <opencv2/opencv.hpp>

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include "nfd.h"

#include "Environment/Environment.h"

int main( int argc, char* argv[] )
{
    Environment env;
    env.launch();

    /*
    nfdchar_t *outPath = NULL;
    nfdresult_t result = NFD_OpenDialog( NULL, NULL, &outPath );
        
    if ( result != NFD_OKAY )
    {
        std::cerr << "You have to select an image to work with." << std::endl;
        return 0;
    }
    
    cv::Mat image = cv::imread( outPath, cv::IMREAD_COLOR );
    if( image.empty() ){
        return -1;
    }
    cv::cvtColor( image, image, cv::COLOR_BGR2RGBA );

    if( !glfwInit() ){
        return -1;
    }

    GLFWwindow* window = glfwCreateWindow( 800, 600, "glfw window", nullptr, nullptr );
    glfwMakeContextCurrent( window );
    glfwSwapInterval( 1 );
    gl3wInit();

    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForOpenGL( window, true );
    ImGui_ImplOpenGL3_Init( "#version 330" );

    GLuint texture;
    glGenTextures( 1, &texture );
    glBindTexture( GL_TEXTURE_2D, texture );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR );
    glTexParameteri( GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR );
    glPixelStorei( GL_UNPACK_ROW_LENGTH, 0 );
    glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, image.cols, image.rows, 0, GL_RGBA, GL_UNSIGNED_BYTE, image.data );

    bool is_show = true;
    while( is_show && !glfwWindowShouldClose(window)){
        glfwPollEvents();
        glClearColor( 0.0f, 0.0f, 0.0f, 1.0f );
        glClear( GL_COLOR_BUFFER_BIT );

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        ImGui::Begin( "imgui image", &is_show );
        glBindTexture( GL_TEXTURE_2D, texture );
        ImGui::Image( reinterpret_cast<void*>( static_cast<intptr_t>( texture ) ), ImVec2( (float)image.cols, (float)image.rows ) );
        ImGui::End();

        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData( ImGui::GetDrawData() );

        glfwSwapBuffers( window );
    }

    ImGui_ImplGlfw_Shutdown();
    ImGui_ImplOpenGL3_Shutdown();
    ImGui::DestroyContext();
    glfwTerminate();
    */

    return 0;
}