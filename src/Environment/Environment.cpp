#include "Environment/Environment.h"

#include <cstdlib>
#include <iostream>

#include "State/State.h"
#include "State/MainState.h"

Environment::Environment()
{

}

Environment::~Environment()
{
    ImGui_ImplGlfw_Shutdown();
    ImGui_ImplOpenGL3_Shutdown();
    ImGui::DestroyContext();
    glfwTerminate();
}

void Environment::launch()
{
    init();

    while(!m_ShouldStop && !glfwWindowShouldClose(m_Handle))
    {
        update();

        beginFrame();
        renderImgui();
        endFrame();
        
        checkStates();
    }
}

void Environment::init()
{
    // GLFW init
    if(!glfwInit())
        std::exit(EXIT_FAILURE);

    // GLFW window init
    m_Handle = glfwCreateWindow(800, 600, "Image processing", nullptr, nullptr);
    glfwMaximizeWindow(m_Handle);
    glfwMakeContextCurrent(m_Handle);
    glfwSwapInterval(1);

    // gl3w init
    gl3wInit();

    // ImGui init
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();

    ImGui_ImplGlfw_InitForOpenGL(m_Handle, true);
    ImGui_ImplOpenGL3_Init("#version 330");

    // State init
    m_States.push(std::make_unique<MainState>());
}

void Environment::update()
{
    m_States.top()->update();
}

void Environment::beginFrame()
{
    glfwPollEvents();
    glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT);

    ImGui_ImplOpenGL3_NewFrame();
    ImGui_ImplGlfw_NewFrame();
    ImGui::NewFrame();
}

void Environment::renderImgui()
{
    m_States.top()->renderImgui();
}

void Environment::endFrame()
{
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

    glfwSwapBuffers(m_Handle);
}

void Environment::checkStates()
{
    if (m_States.top()->isPendingKilled())
        m_States.pop();

    if (m_States.empty())
    {
        m_ShouldStop = true;
        return;
    }

    if (m_States.top()->hasPushedState())
        m_States.push(m_States.top()->consumePushedState());
}