#pragma once

#include "imgui.h"
#include "imgui_impl_glfw.h"
#include "imgui_impl_opengl3.h"
#include <GL/gl3w.h>
#include <GLFW/glfw3.h>

#include <stack>
#include <memory>

class State;

/**
 * @brief Main class of the application. Manages states and main loop steps.
 * 
 */
class Environment
{
public:
    Environment();
    ~Environment();

    void launch();

private:
    void init();
    void update();
    void beginFrame();
    void renderImgui();
    void endFrame();
    void checkStates();

private:
    GLFWwindow* m_Handle = nullptr;
    bool m_ShouldStop = false;

    std::stack<std::unique_ptr<State>> m_States;
};