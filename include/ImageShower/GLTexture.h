#pragma once

class GLTexture
{
public:
    GLTexture();
    GLTexture(unsigned int width, unsigned int height, const void* data);
    GLTexture(GLTexture&& other);
    ~GLTexture();

    void create(unsigned int width, unsigned int height, const void* data);

    unsigned int getID() const;
    void bind() const;

private:
    unsigned int m_RendererID = 0;
    unsigned int m_Width = 0, m_Height = 0;
};