#pragma once

#include <opencv2/opencv.hpp>
#include <string>
#include <list>

#include "ImageShower/Image.h"

class ImageShower
{
public:
    ImageShower();

    void addImage(const cv::Mat& image, const std::string& name, bool canBeClosed = true);
    void clear();

    void renderImgui();

private:
    std::list<Image> m_Images;
};