#pragma once

#include <opencv2/opencv.hpp>

#include "ImageShower/GLTexture.h"

class Image
{
public:
    Image(const cv::Mat& image, const std::string& name, bool canBeClosed = true);
    Image(Image&& other);
    ~Image();

    const cv::Mat& getData() const;
    const std::string& getName() const;
    bool shouldClose() const;

    void renderImgui() const;

private:
    cv::Mat m_Data;
    std::string m_Name;
    GLTexture m_Texture;
    mutable bool m_Open = true;
    bool m_CanBeClosed;
};