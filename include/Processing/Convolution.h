#pragma once

#include <opencv2/opencv.hpp>
#include <functional>

# define M_PI           3.14159265358979323846  /* pi */

class Convolution
{
public:
    using Pixel1Operand = std::function<float(float)>;
    using Pixel2Operand = std::function<float(float, float)>;
    using Pixel2Operand2C = std::function<cv::Vec2f(float, float)>;
    using Pixel4Operand2C = std::function<cv::Vec2f(float, float, float, float)>;
public:
    static cv::Mat Convolve(const cv::Mat& image, const cv::Mat& filter);
    static cv::Mat OperatePixelPerPixel(const cv::Mat& image1, const cv::Mat& image2, const Pixel2Operand& f);
    static cv::Mat OperatePixelPerPixel2C(const cv::Mat& image1, const cv::Mat& image2, const Pixel2Operand2C& f);
    static cv::Mat OperatePixelPerPixel2C(const cv::Mat& image1, const cv::Mat& image2, const cv::Mat& image3, const cv::Mat& image4, const Pixel4Operand2C& f);
    static cv::Mat OperatePixelPerPixel(const cv::Mat& image, const Pixel1Operand& f);

    static cv::Mat UcharToFloat(const cv::Mat& image);
    static cv::Mat FloatToUchar(const cv::Mat& image);

    static float EuclideanNorm(float left, float right);
    static cv::Vec2f EuclideanNormAndSlope(float x, float y);
    static float Max(float left, float right);
    static cv::Vec2f MaxAndSlope(float px_0, float px_45, float px_90, float px_135);
    static float ManhattanDistance(float left, float right);

    static float ClampPixel(float pixel, float min, float max);
    static float ClampDown(float pixel, float min);
    static float ClampUp(float pixel, float max);
    static float Treshold(float pixel, float treshold, float lowValue, float highValue);

public:
    static const cv::Mat PREWITT_FILTER_0;
    static const cv::Mat PREWITT_FILTER_45;
    static const cv::Mat PREWITT_FILTER_90;
    static const cv::Mat PREWITT_FILTER_135;
    static const cv::Mat SOBEL_FILTER_0;
    static const cv::Mat SOBEL_FILTER_45;
    static const cv::Mat SOBEL_FILTER_90;
    static const cv::Mat SOBEL_FILTER_135;
    static const cv::Mat KIRSCH_FILTER_0;
    static const cv::Mat KIRSCH_FILTER_45;
    static const cv::Mat KIRSCH_FILTER_90;
    static const cv::Mat KIRSCH_FILTER_135;
};