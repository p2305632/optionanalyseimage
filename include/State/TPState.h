#pragma once

#include "State/State.h"
#include <opencv2/opencv.hpp>

#include "ImageShower/ImageShower.h"

class TPState : public State
{
public:
    TPState(const std::string& stateName = "");

    void onRenderImgui() override;

protected:
    void showImage(const cv::Mat& image, const std::string& name, bool canBeClosed = true);

private:
    void openImage();

protected:
    bool m_ImageLoaded = false;
    cv::Mat m_ImportedImage;

private:
    ImageShower m_ImageShower;

};