#pragma once

#include <memory>
#include <string>

/**
 * @brief A state is everything shown on the window.
 * You need to redefine "void onUpdate()" and "void onRenderImgui()" pure virtual functions.
 * 
 */
class State
{
public:
    State(const std::string& stateName = "");

    bool hasPushedState() const;
    std::unique_ptr<State> consumePushedState();

    bool isPendingKilled() const;

    void update();
    void renderImgui();

protected:

    template <typename S, typename... Args>
    void pushState(Args... args)
    {
        m_PushedState = std::make_unique<S>(args...);
    }

    void kill();

    virtual void onUpdate() = 0;
    virtual void onRenderImgui() = 0;

private:
    std::unique_ptr<State> m_PushedState = nullptr;
    bool m_IsPendingKilled = false;
    std::string m_StateName = "";
};