#pragma once

#include "State/TPState.h"
#include <opencv2/opencv.hpp>

#include "Processing/Convolution.h"

class TP1State : public TPState
{
public:
    TP1State();

    void onUpdate() override;
    void onRenderImgui() override;

private:
    void edgesDetection2D(
        const cv::Mat& filter1,
        const cv::Mat& filter2,
        const Convolution::Pixel2Operand2C& op,
        const std::string& imageName
    );
    void edgesDetection4D(
        const cv::Mat& filter1,
        const cv::Mat& filter2,
        const cv::Mat& filter3,
        const cv::Mat& filter4,
        const Convolution::Pixel4Operand2C& op,
        const std::string& imageName
    );

private:
    bool m_UseTreshold2D = false, m_UseTreshold4D = false;
    bool m_ColorWithSlopes4D = false;
    float m_FiltersTreshold2D = 20.f, m_FiltersTreshold4D = 20.f;
};