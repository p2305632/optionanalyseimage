#pragma once

#include "State/State.h"

class MainState : public State
{
public:
    MainState();
    ~MainState();

    void onUpdate() override;
    void onRenderImgui() override;

private:
    void launchTP(int id);

};