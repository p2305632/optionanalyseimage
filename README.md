# OptionAnalyseImage

## Dependencies

- CMake
- Git
- OpenGL
- OpenCV

You need to add `...\OpenCV\build` and `...\OpenCV\build\x64\vc16\bin` from your OpenCV library installation to your system path :
- Search for "path" in Windows start menu.
- Select "Edit environment variables".
- Select "Environment variables...".
- In "System variables", double click on "Path".
- Add the two paths from you OpenCV installation to the list.

If you are compiling with vscode and already launched it, you will need to restart the software to update path variable (next instructions are for command line compiling).

## Build instructions

Build has only be tested on Windows with VS Code and MSVC Compiler (VC++ 17).

Clone the project repository on your system:
```
git@forge.univ-lyon1.fr:p2305632/optionanalyseimage.git
cd optionalayseimage
```

Create a build folder and navigate inside it:
```
mkdir build
cd build
```

Generate Visual Studio solution and compile it:
```
cmake .. 
cmake --build .
```

Build files are written in build/Debug/.